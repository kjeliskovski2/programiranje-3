import mpi.MPI;

import java.awt.*;

public class GUI {
    int n;            // Number of particles
    int t;            // Total time of the simulation
    int dt;           // Timestamp
    long start;       // Starting time
    boolean graphics; // Switches GUI on and off

    public GUI(int n, int t, int dt, boolean graphics) { // Constructor
        this.n = n;
        this.t = t;
        this.dt = dt;
        this.graphics = graphics;
    }

    @SuppressWarnings("deprecation")
    public void run() {
        int rank = MPI.COMM_WORLD.Rank(); // Getting the rank
        int size = MPI.COMM_WORLD.Size(); // Getting the size of the cluster
        start = System.currentTimeMillis(); // Starting time of the simulation

        Simulation simulation = new Simulation();
        Particles[] p = simulation.readParticles(this.n);  // Creating n particles

        // Distribute particles among processors
        Particles[] subParticles = distributeParticles(p); // Getting a subarray

        if (graphics && rank == 0) {
            // Code executed by only one process to set up graphics
            StdDraw.setScale(-8200, 8800);
            StdDraw.picture(0, 0, "../img/background.png");
            StdDraw.setTitle("Particles-Distributed version");
            StdDraw.show();  // Show the graphics after setting up
        }


        MPI.COMM_WORLD.Barrier(); // Make sure all processes reach this point before continuing

        for (int t = 0; t <= this.t; t += this.dt) {

            double[] xForces = new double[subParticles.length];
            double[] yForces = new double[subParticles.length];

            for (int i = 0; i < subParticles.length; i++) {
                xForces[i] = subParticles[i].calcNetForceX(p); // Calculate forces by X on subParticles assigned to this process
                yForces[i] = subParticles[i].calcNetForceY(p); // Calculate forces by Y on subParticles assigned to this process
            }


            double[] xForcesAll = gatherForces(xForces);  // Gather forces by X from all processes to process 0
            double[] yForcesAll = gatherForces(yForces);  // Gather forces by Y from all processes to process 0


            for (int i = 0; i < subParticles.length; i++) {
                subParticles[i].update(dt, xForcesAll[i], yForcesAll[i]); // Update subParticles based on gathered forces
            }

            Particles[] updatedParticles = gatherParticles(subParticles, size); // Gather all of the updated particles

            if (graphics && rank == 0) {
                StdDraw.picture(0, 0, "../img/background.png");
                for (int i = 0; i < updatedParticles.length; i++) {
                    updatedParticles[i].draw(); // Code executed by only process 0 for rendering
                }
                StdDraw.show(5);
            }
        }

        if(rank == 0) {
            System.out.println("Distributed version takes: " + (System.currentTimeMillis() - start) + " ms");
        }
    }

    private Particles[] distributeParticles(Particles[] particles) {
        int rank = MPI.COMM_WORLD.Rank(); // Getting the rank
        int size = MPI.COMM_WORLD.Size(); // Getting the size of the cluster
        int particlesPerProcess = particles.length / size; // Dividing the amount of particles
        int remainder = particles.length % size;
        int localParticles = (rank < remainder) ? particlesPerProcess + 1 : particlesPerProcess; // Calculate the number of particles each process will receive
        int startIndex = rank * particlesPerProcess + Math.min(rank, remainder); // Calculate the starting index for each process

        Particles[] localParticlesArray = new Particles[localParticles]; // Create a subarray to hold the particles assigned to this process

        // Perform the scatter operation to distribute particles among processes
        MPI.COMM_WORLD.Scatter(
                particles,       // Source array (only used in root process)
                startIndex,      // Starting index for this process
                localParticles,  // Number of elements to send to each process
                MPI.OBJECT,       // Data type of the elements
                localParticlesArray,  // Destination array in this process
                0,               // Starting index in the destination array
                localParticles,  // Number of elements to receive
                MPI.OBJECT,       // Data type of the elements
                0                // Rank of the root process
        );

        return localParticlesArray;
    }

    private double[] gatherForces(double[] localForces) {
        double[] gatheredForces;
        gatheredForces = new double[localForces.length * MPI.COMM_WORLD.Size()]; // Create an array to hold the gathered forces

        // Perform the Gather operation to gather forces from all processes
        MPI.COMM_WORLD.Gather(
                localForces,        // Data to send (local forces in each process)
                0,                  // Starting index in the send buffer
                localForces.length, // Number of elements to send from each process
                MPI.DOUBLE,         // Data type of the elements
                gatheredForces,     // Receive buffer in all processes
                0,                  // Starting index in the recieve buffer
                localForces.length, // Number of elements to receive in all processes
                MPI.DOUBLE,         // Data type of the elements
                0                   // Rank of the root process
        );

        return gatheredForces;
    }

    private Particles[] gatherParticles(Particles[] localParticles, int size) {
        Particles[] gatheredParticles;
        gatheredParticles = new Particles[localParticles.length * MPI.COMM_WORLD.Size()]; // Creating array to hold the particles

        // Perform the Gather operation to gather particles from all processes
        MPI.COMM_WORLD.Gather(
                localParticles,   // Data to send (local particles in each process)
                0,                // Starting index in the send buffer
                localParticles.length,  // Number of elements to send
                MPI.OBJECT,       // Data type of the elements
                gatheredParticles, // Receive buffer in root process (null because rank != 0)
                0,                // Starting index in the receive buffer
                localParticles.length, // Number of elements to receive in root process
                MPI.OBJECT,       // Data type of the elements
                0                 // Rank of the root process
        );

        return gatheredParticles;
    }
}
