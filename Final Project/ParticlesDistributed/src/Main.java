import mpi.MPI;

public class Main {

    public static void main(String[] args) {
        MPI.Init(args); // Initializing MPI

        try {
            int n = 200;
            int t = 10000;
            int dt = 10;

            GUI gui = new GUI(n, t, dt, true); // Creating an instance of GUI class
            gui.run();
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
            e.printStackTrace();
        } finally {
            MPI.Finalize(); // Closing MPI
        }
    }
}
