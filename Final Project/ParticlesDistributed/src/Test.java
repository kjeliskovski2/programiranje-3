import java.util.*;
import mpi.*;

public class Test {
    private static int id;
    private static int size;
    private static final int n = 10;
    private static int[] a = new int[n];

    /**
     * @param args
     */
    public static void main(String[] args) {
        MPI.Init(args);
        id = MPI.COMM_WORLD.Rank();
        size = MPI.COMM_WORLD.Size();
        System.out.println("jaz sem "+id+" od "+size);

        Random rand = new Random(id+System.currentTimeMillis());
        for (int i = 0; i < a.length; i++)
            a[i] = rand.nextInt(11);
        Arrays.sort(a);
        System.out.println("["+id+"] "+Arrays.toString(a));

        if (id == 0)
            coordinate();
        else
            work();

        MPI.Finalize();
    }

    private static void coordinate() {
        int[] b = new int[1];
        for (int i = 0; i < a.length; i++) {
            MPI.COMM_WORLD.Bcast(a, i, 1, MPI.INT, 0);
            boolean common = true;
            for (int p = 1; p < size; p++) {
                MPI.COMM_WORLD.Recv(b, 0, 1, MPI.INT, MPI.ANY_SOURCE, 1);
                if (b[0] != a[i])
                    common = false;
            }
            b[0] = common ? -2 : -1;
            MPI.COMM_WORLD.Bcast(b, 0, 1, MPI.INT, 0);
            if (common) {
                System.out.println("["+id+"] skupni najmanjsi element: "+a[i]);
                return;
            }
        }
        b[0] = -3;
        MPI.COMM_WORLD.Bcast(b, 0, 1, MPI.INT, 0);
        System.out.println("["+id+"] nimamo skupnega elementa");
    }

    private static void work() {
        int[] b = new int[1];
        for (;;) {
            MPI.COMM_WORLD.Bcast(b, 0, 1, MPI.INT, 0);
            if (b[0] == -3) {
                System.out.println("["+id+"] nimamo skupnega elementa");
                return;
            }
            int x = b[0];
            int index = Arrays.binarySearch(a, x);
            b[0] = index < 0 ? -1 : x;
            MPI.COMM_WORLD.Send(b, 0, 1, MPI.INT, 0, 1);
            MPI.COMM_WORLD.Bcast(b, 0, 1, MPI.INT, 0);
            if (b[0] == -2) {
                System.out.println("["+id+"] skupni najmanjsi element: "+x);
                return;
            }
        }
    }

}