import java.awt.*;
import java.io.Serializable;
import java.util.Random;

class Particles implements Serializable {
    double xPos;   // Position by X
    double yPos;   // Position by Y
    double xVel;   // Velocity by X
    double yVel;   // Velocity by Y
    double mass;   // Mass
    double charge; // Charge

    public Particles(double xPos, double yPos, double xVel, double yVel, double mass, double charge) {
        // Constructor of the particle
        this.xPos   = xPos;
        this.yPos   = yPos;
        this.xVel   = xVel;
        this.yVel   = yVel;
        this.mass   = mass;
        this.charge = charge;
    }

    public double calcDistance(Particles p) {
        // The eucledian distance is calculated by this formula r = sqrt(x^2+y^2)
        return Math.pow((this.xPos - p.xPos), 2) + Math.pow((this.yPos - p.yPos), 2);
    }

    public double calcForce(Particles p) {
        // The formula for force by Coulombs law is F = |q1*q2| / r^2
        double r = this.calcDistance(p);  // The distance between the particles

        return ((Math.abs(this.charge*p.charge))/r);
    }

    public double calcForceX(Particles p ){
        // The force on x axis is calculated by this formula F1,x = F * Dx/r^2
        double f  = this.calcForce(p);    // The force between them
        double dx = this.xPos - p.xPos;   // Dx - difference of distance by X
        double r  = this.calcDistance(p); // Eucledian distance

        return ((f*dx)/r);
    }

    public double calcForceY(Particles p) {
        // The force on y axis is calculated by this formula F1,y = F * Dy/r^2
        double f  = this.calcForce(p);    // The force between them
        double dy = this.yPos - p.yPos;   // Dy - difference of distance by Y
        double r  = this.calcDistance(p); // Eucledian distance

        return ((f*dy)/r);
    }

    public double calcNetForceX(Particles[] allparticles) {
        // Calculating the total forces on the X axis, it is calculated as a sum of all the forces
        double sumX = 0.0;

        for (int i = 0; i < allparticles.length; i++) {
            if(!allparticles[i].equals(this)) {      // The particle can not attract itself
                sumX += calcForceX(allparticles[i]); // Sum of all forces on X-axis
            }
        }

        return sumX;
    }

    public double calcNetForceY(Particles[] allparticles) {
        // Calculating the total forces on the Y axis, it is calculated as a sum of all the forces
        double sumY = 0.0;

        for (int i = 0; i < allparticles.length; i++) {
            if(!allparticles[i].equals(this)) {      // The particle can not attract itself
                sumY += calcForceY(allparticles[i]); // Sum of all forces on Y-axis
            }
        }

        return sumY;
    }

    public void update(double secs, double xforce, double yforce) {
        // After all the forces are calculated we update the particle's position
        double accX = xforce / this.mass;           // Acceleration by X
        double accY = yforce / this.mass;           // Acceleration by Y
        this.xVel   = this.xVel + secs * accX;      // Velocity by X
        this.yVel   = this.yVel + secs * accY;      // Velocity by Y
        this.xPos   = this.xPos + secs * this.xVel; // X axis
        this.yPos   = this.yPos + secs * this.yVel; // y axis
    }

    public void draw() {
        if(this.charge < 0) {
            StdDraw.setPenColor(Color.blue);             // Positive particle
            StdDraw.filledCircle(xPos, yPos, 90); // Circle with position xPos and yPos and a radius of 100
        }
        else {
            StdDraw.setPenColor(Color.red);              // Negative particle
            StdDraw.filledCircle(xPos, yPos, 90);  // Circle with position xPos and yPos and a radius of 100
        }
    }
}