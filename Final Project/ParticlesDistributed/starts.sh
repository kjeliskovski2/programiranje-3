#!/bin/bash


# Compile the program
javac -cp .:./lib/mpj-v0_44/lib/mpj.jar:./lib/stdlib.jar ./src/Main.java ./src/GUI.java ./src/Simulation.java ./src/Particles.java 

# Check if compilation was successful before attempting to run
if [ $? -eq 0 ]; then
  # Run the program
  cd "src/"
  ../lib/mpj-v0_44/bin/mpjrun.sh -cp .:../lib/mpj-v0_44/lib/mpj.jar:../lib/stdlib.jar Main
else
  echo "Compilation failed. Exiting..."
fi


