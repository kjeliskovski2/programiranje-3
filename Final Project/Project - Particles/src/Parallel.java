import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class Parallel {
    int n;            // Number of particles
    int t;            // Total time of the simulation
    int dt;           // Timestamp
    long start;       // Starting time
    boolean graphics; // Switches GUI on and off
    public Parallel(int n, int t, int dt, boolean graphics) {
        this.n  = n;
        this.t  = t;
        this.dt = dt;
        this. graphics = graphics;
    }
    public void runParallel() {
        int psize = this.n;                     // How many particles do we have
        start     = System.currentTimeMillis(); // Starting time of the simulation

        Simulation simulation = new Simulation();
        Particles[] p         = simulation.readParticles(this.n);  // Creating n particles

        if (graphics) { // If the user selects graphics we turn them on, else we dont
            StdDraw.setScale(-8200, 8800);
            StdDraw.picture(0, 0, "img/background.png");
            StdDraw.setTitle("Particles-Parallel version");
        }

        // New executor service with fixed ThreadPool of n processors
        ExecutorService executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

        try {
            for (int t = 0; t <= this.t; t += this.dt) {
                double[] xForces = new double[p.length]; // Initiating an array for the values of X-Forces for each particle
                double[] yForces = new double[p.length]; // Initiating an array for the values of Y-Forces for each particle

                // Create a list to hold the tasks
                List<Callable<Void>> tasks = new ArrayList<>(); // Creating a list of type Callable for holding the tasks

                for (int i = 0; i < psize; i++) {
                    final int index = i;
                    tasks.add(() -> { // Adding the tasks
                        xForces[index] = p[index].calcNetForceX(p); // Storing the values of X-Forces for each particle
                        yForces[index] = p[index].calcNetForceY(p); // Storing the values of Y-Forces for each particle
                        return null;
                    });
                }

                // Invoke all tasks in parallel
                List<Future<Void>> futures = executorService.invokeAll(tasks); // Adding the tasks to an executor service



                for (Future<Void> future : futures) {
                    future.get(); // Wait for all parallel computations to finish before rendering
                }

                // Create a list to hold the update tasks
                List<Callable<Void>> updateTasks = new ArrayList<>(); // Creating another Callable list to for updating

                for (int i = 0; i < p.length; i++) {
                    final int particleIndex = i;
                    updateTasks.add(() -> { // Adding the tasks for updating
                        p[particleIndex].update(dt, xForces[particleIndex], yForces[particleIndex]);
                        return null;
                    });
                }

                // Invoke all update tasks in parallel
                List<Future<Void>> updateFutures = executorService.invokeAll(updateTasks); // Adding the task to an executor service


                for (Future<Void> updateFuture : updateFutures) {
                    updateFuture.get(); // Wait for all parallel updates to finish
                }

                if (graphics) {
                    StdDraw.picture(0, 0, "img/background.png"); // Rendering should be done in a single thread to avoid conflicts

                    for (int i = 0; i < psize; i++) {
                        p[i].draw(); // Draw the particles
                    }
                    StdDraw.show(5); // Render
                }
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        } finally {
            executorService.shutdown(); // We don't need the executorService anymore
        }

        System.out.println("Parallel version takes: "+(System.currentTimeMillis()-start)+" ms");
    }
}
