/*
            PROJECT PARTICLES - PROGRAMIRANJE 3

    TITLE:   PARTICLES
    AUTHOR:  KRISTIJAN JELISKOVSKI
    STUDENT  NUMBER: 89211067
    PROGRAM: COMPUTER SCIENCE - SLOVENIAN GROUP
    LEGEND: Blue particle-negative charge, Red particle-positive charge. Same charge particles repel, opposite attract.
 */

import javax.swing.*;
import java.util.Scanner;

class main {
    public static void main(String[] args) {
        int n;       // Total number of particles
        int t;       // Total simulation time
        int dt;      // Timestamp
        int version; // Version of the program

        Scanner scanner = new Scanner(System.in);

        System.out.println("Choose version:");
        System.out.println("1. Sequential & GUI 2. Sequential & no GUI");
        System.out.println("3. Parallel & GUI 4. Parallel & no GUI");
        version = scanner.nextInt();

        if (version>5 || version<1) {System.out.println("Unrecognised option, try again."); System.exit(3);};

        System.out.println("Enter the number of particles(ex. 100): ");
        n = scanner.nextInt();
        System.out.println("Enter the total time(ex.10000): ");
        t = scanner.nextInt();
        System.out.println("Enter the timestamp(ex.10): ");
        dt = scanner.nextInt();

        if (version == 1) {GUI seq = new GUI(n,t,dt,true); seq.run();}
        else if (version == 2) {GUI seq = new GUI(n,t,dt,false); seq.run();}
        else if (version == 3) {Parallel par2 = new Parallel(n,t,dt,true); par2.runParallel();}
        else if (version == 4) {Parallel par2 = new Parallel(n,t,dt,false); par2.runParallel();}





    }
}