import java.util.Random;
import java.util.Scanner;

public class Simulation {

    public Particles[] readParticles(int n) {
        // Creating the particles
        Particles[] p = new Particles[n];

        for (int i = 0; i < n; i++) {
                // ALL NUMBERS THAT ARE USED ARE GENERATED FROM PREVIOUS TESTS TO GET GOOD ATTRACTION FORCE
                // BECAUSE OF THE EQUATIONS USED I CAME UP WITH THESE NUMBERS TO GIVE THE PARTICLES STRENGTH

                Random r = new Random();
                double xPos   = r.nextDouble(-8000,8000);    // The position by X, bounded by the radius
                double yPos   = r.nextDouble(-8700,8700);    // The position by Y, bounded by the radius
                double xVel   = 0;                                        // Initial velocity by X
                double yVel   = 0;                                        // Initial velocity by Y
                double mass   = 200;                                      // Mass of the particle
                double charge = r.nextDouble(-15000, 15000); // The electric charge

            p[i] = new Particles(xPos,yPos, xVel, yVel, mass, charge);    // Creating a particle object
        }
        return p;
    }
}
