/*
            PROJECT PARTICLES - PROGRAMIRANJE 3

    TITLE:   PARTICLES
    AUTHOR:  KRISTIJAN JELISKOVSKI
    STUDENT  NUMBER: 89211067
    PROGRAM: COMPUTER SCIENCE - SLOVENIAN GROUP
    LEGEND: Blue particle-negative charge, Red particle-positive charge. Same charge particles repel, opposite attract.
 */

import javax.swing.*;
import java.util.Scanner;

class main {
    public static void main(String[] args) {
        int n;       // Total number of particles
        int t;       // Total simulation time
        int dt;      // Timestamp
        int version; // Version of the program

        Scanner scanner = new Scanner(System.in);

        /*System.out.println("Choose version:");
        System.out.println("1. Sequential & GUI 2. Sequential & no GUI");
        System.out.println("3. Parallel & GUI 4. Parallel & no GUI");
        System.out.println("5. Distributed & GUI 6. Distributed & no GUI");
        version = scanner.nextInt();

        if (version>6 || version<1) {System.out.println("Unrecognised option, try again."); System.exit(3);};

        System.out.println("Enter the number of particles(ex. 100): ");
        n = scanner.nextInt();
        System.out.println("Enter the total time(ex.10000): ");
        t = scanner.nextInt();
        System.out.println("Enter the timestamp(ex.10): ");
        dt = scanner.nextInt();

        if (version == 1) {GUI seq = new GUI(n,t,dt,true); seq.run();}
        else if (version == 2) {GUI seq = new GUI(n,t,dt,false); seq.run();}
        else if (version == 3) {GUIParalel par = new GUIParalel(n,t,dt,true); par.runParallel();}
        else if (version == 4) {GUIParalel par = new GUIParalel(n,t,dt,false); par.runParallel();} */

        GUI_Start guiStart = new GUI_Start();
        guiStart.start();




    }
}