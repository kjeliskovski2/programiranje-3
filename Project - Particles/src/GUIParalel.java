import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class GUIParalel {
    int n;            // Number of particles
    int t;            // Total time of the simulation
    int dt;           // Timestamp
    long start;       // Starting time
    boolean graphics; // Switches GUI on and off
    public GUIParalel(int n, int t, int dt, boolean graphics) {
        this.n  = n;
        this.t  = t;
        this.dt = dt;
        this. graphics = graphics;
    }
    public void runParallel() {
        int psize = this.n;                     // How many particles do we have
        start     = System.currentTimeMillis(); // Starting time of the simulation

        Simulation simulation = new Simulation();
        Particles[] p         = simulation.readParticles(this.n);  // Creating n particles

        if (graphics == true) { // If the user selects graphics we turn them on, else we dont
            StdDraw.setScale(-8200, 8800);
            StdDraw.picture(0, 0, "img/background.png");
            StdDraw.setTitle("Particles-Parallel version");
        }

        // New executor service with fixed ThreadPool of n processors
        ExecutorService executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

        try {
            for (int t = 0; t <= this.t; t += this.dt) {
                double[] xForces = new double[p.length]; // Initiating an array for the values of X-Forces for each particle
                double[] yForces = new double[p.length]; // Initiating an array for the values of Y-Forces for each particle

                // Create a list to hold the tasks
                List<Callable<Void>> tasks = new ArrayList<>();

                for (int i = 0; i < psize; i++) {
                    final int index = i;
                    tasks.add(() -> { // Adding the tasks
                        xForces[index] = p[index].calcNetForceX(p); // Storing the values of X-Forces for each particle
                        yForces[index] = p[index].calcNetForceY(p); // Storing the values of Y-Forces for each particle
                        return null;
                    });
                }

                // Invoke all tasks in parallel
                List<Future<Void>> futures = executorService.invokeAll(tasks);


                // Wait for all parallel computations to finish before rendering
                for (Future<Void> future : futures) {
                    future.get();
                }

                for (int i = 0; i < psize; i++) {
                    p[i].update(dt, xForces[i], yForces[i]); // Updating the particles
                }

                if (graphics == true) {
                    // Rendering should be done in a single thread to avoid conflicts
                    StdDraw.picture(0, 0, "img/background.png");

                    // Draw the particles
                    for (int i = 0; i < psize; i++) {
                        p[i].draw();
                    }

                    StdDraw.show(5); // Render
                }
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        } finally {
            executorService.shutdown(); // We don't need the executorService anymore
        }

        System.out.println("Parallel version takes: "+(System.currentTimeMillis()-start)+" ms");
    }
}
