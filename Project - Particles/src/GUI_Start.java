import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GUI_Start {

    int n;            // Number of particles
    int version = 0;
    int t;            // Total time of the simulation
    int dt;           // Timestamp
    long startT;       // Starting time
    boolean graphics; // Switches GUI on and off

    JFrame jFrame = new JFrame();
    public void start() {

        Poslusalec poslusalec = new Poslusalec(jFrame);

        jFrame.setSize(700,400);

        JButton start = new JButton("Start");
        start.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jFrame.dispose();
                if (version == 1) {GUI guiS = new GUI(100,10000,10,graphics); guiS.run();}
                else if (version == 2) {GUIParalel guiP = new GUIParalel(100,10000,10,graphics); guiP.runParallel();}
            }
        });
        JButton yes   = new JButton("Yes");
        yes.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                graphics = true;
            }
        });
        JButton no    = new JButton("No");
        no.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                graphics = false;
            }
        });
        JButton seq   = new JButton("Sequential");
        seq.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                version = 1;
            }
        });
        JButton par   = new JButton("Parallel");
        par.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                version = 2;
            }
        });
        JButton dis   = new JButton("Distributed");
        dis.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                version = 3;
            }
        });

        JTextField part  = new JTextField(3);
        JTextField time  = new JTextField(5);
        JTextField tstmp = new JTextField(3);

        JLabel gui   = new JLabel("Do you want GUI?");
        JLabel vers  = new JLabel("Choose your version:");
        JLabel data1 = new JLabel("No. of particles(ex. 100)");
        JLabel data2 = new JLabel("Total time(ex. 10 000)");
        JLabel data3 = new JLabel("Timestamp(ex. 10)");

        JPanel versPanel  = new JPanel();
        JPanel versBPanel = new JPanel();
        JPanel guiPanel   = new JPanel();
        JPanel guiBPanel  = new JPanel();
        JPanel dataPanel  = new JPanel();
        JPanel dataBPanel = new JPanel();
        JPanel startPanel = new JPanel();

        versPanel.add(vers);
        versBPanel.add(seq);
        versBPanel.add(par);
        versBPanel.add(dis);

        guiPanel.add(gui);
        guiBPanel.add(yes);
        guiBPanel.add(no);

        dataBPanel.add(data1);
        dataBPanel.add(part);
        dataBPanel.add(data2);
        dataBPanel.add(time);
        dataBPanel.add(data3);
        dataBPanel.add(tstmp);

        startPanel.add(start);

        JPanel panel = new JPanel(new GridLayout(0,1));
        panel.add(versPanel);
        panel.add(versBPanel);
        panel.add(guiPanel);
        panel.add(guiBPanel);
        panel.add(dataBPanel);
        panel.add(startPanel);

        jFrame.add(panel);
        jFrame.setDefaultCloseOperation(3);
        jFrame.setVisible(true);



    }
}
