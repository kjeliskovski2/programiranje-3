import java.awt.*;

public class GUI {
    int n;            // Number of particles
    int t;            // Total time of the simulation
    int dt;           // Timestamp
    long start;       // Starting time
    boolean graphics; // Switches GUI on and off
    public GUI(int n, int t, int dt, boolean graphics) {
        this.n  = n;
        this.t  = t;
        this.dt = dt;
        this.graphics = graphics;
    }


    public void run() {
        int psize = this.n;                 // How many particles do we have
        start = System.currentTimeMillis(); // Starting time of the simulation

        Simulation simulation = new Simulation();
        Particles[] p         = simulation.readParticles(this.n);  // Creating n particles

        if(graphics == true) {
            StdDraw.setScale(-8200, 8800);// Creating a universe with radius 8200x8800
            StdDraw.picture(0, 0, "img/background.png"); // Drawing black background
            StdDraw.setTitle("Particles-Sequential version");
        }


        for (int t = 0; t <= this.t; t += this.dt ) {
            double[] xForces = new double[p.length];        // Array of forces on X-axis for each particle
            double[] yForces = new double[p.length];        // Array of forces on Y-axis for each particle

            for (int i = 0; i < psize; i++) {
                xForces[i] = p[i].calcNetForceX(p);          // Calculating the net X force for every particle on the timestamp
                yForces[i] = p[i].calcNetForceY(p);          // Calculating the net Y force for every particle on the timestamp

            }

            for (int i = 0; i < psize; i++) {
                p[i].update(dt, xForces[i], yForces[i]); // Updating their position
            }

            if(graphics == true) {
                StdDraw.picture(0, 0, "img/background.png"); // Drawing black background

                for (int i = 0; i < psize; i++) {
                    p[i].draw(); // Drawing the particles

                }

                StdDraw.show(5); // Rendering
            }


        }

        System.out.println("Sequential version takes: "+(System.currentTimeMillis() - start)+" ms");
    }
}
