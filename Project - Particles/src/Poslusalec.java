import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Poslusalec extends GUI_Start implements ActionListener {

    private JFrame frame;

    public Poslusalec(JFrame frame) {
        this.frame = frame;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JButton clicked = (JButton) e.getSource();

        if(clicked.getText().equals("Yes")) { graphics = true;}
        else if(clicked.getText().equals("No")) { graphics = false;}

        if(clicked.getText().equals("Sequential")) { version = 1;}
        else if(clicked.getText().equals("Parallel")) { version = 2;}
        else if(clicked.getText().equals("Distributed")) { version = 3;}

        if (version != 0) {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    startProgram();
                }
            });
        }
    }

    public void startProgram() {
        frame.dispose();
        if (version == 1) {GUI guiS = new GUI(100,10000,10,graphics); guiS.run();}
        else if (version == 2) {GUIParalel guiP = new GUIParalel(100,10000,10,graphics); guiP.runParallel();}
    }
}
